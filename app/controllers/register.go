package controllers

import "github.com/revel/revel"

type Register struct {
	*revel.Controller
}

func (c Register) Index() revel.Result {
	return c.Render()
}
