package controllers

import (
	"github.com/revel/revel"
	"bankroll/app/models"
)

type User struct {
	*revel.Controller
}

func (c User) List() revel.Result {
	users := getAllUsers()
	return c.Render(users)
}

func (c User) Show(username string) revel.Result {
	user := getUser(username)
	return c.Render(user)
}

func (c User) Create() revel.Result {
	return c.Render()
}

func getUser(username string) *models.User {
	return models.Find(username)
}

func getAllUsers() map[string]*models.User {
	return models.All()
}
