# README #

### What is this repository for? ###

Go Lang + Revel API

0.0.1

### How do I get set up? ###

Get boot2docker installed (or Docker flat out on non-Mac OSX) and run it:

- boot2docker start

Note the IP address returned and make sure to export the line it says.

In the main directory:

- docker build -t <username>/golang
- docker run -t <username>/golang

Navigate to http://DOCKER_HOST:9876

Done.

### Contribution guidelines ###



### Who do I talk to? ###

