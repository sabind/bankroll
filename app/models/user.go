package models

import (
	"fmt"
	"github.com/revel/revel"
	"regexp"
)

type User struct {
	UserId										int
	FirstName, LastName 						string
	Username, Password							string
	HashedPassword								[]byte
}

func (u *User) String() string {
	return fmt.Sprintf("User(%s)", u.Username)
}

var userRegex = regexp.MustCompile("^\\w*$")

func (user *User) Validate(v *revel.Validation) {
	v.Check(user.Username,
		revel.Required{},
		revel.MaxSize{15},
		revel.MinSize{4},
		revel.Match{userRegex},
	)

	ValidatePassword(v, user.Password).
		Key("user.Password")

	v.Check(user.FirstName,
		revel.Required{},
		revel.MaxSize{100},
	)

	v.Check(user.LastName,
		revel.Required{},
		revel.MaxSize{100},
	)
}

func ValidatePassword(v *revel.Validation, password string) *revel.ValidationResult {
	return v.Check(password,
		revel.Required{},
		revel.MaxSize{15},
		revel.MinSize{5},
	)
}

func Find(username string) *User {
	return db[username]
}

func All() map[string] *User {
	return db
}

func FindOrCreate(username string, password string, firstName string, lastName string) *User {
	if user, ok := Find(username); ok {
		return user
	}
	user := &User{
		Username: username,
		Password: password,
		FirstName: firstName,
		LastName: lastName,
	}

	db[username] = user
	return user
}


