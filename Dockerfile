FROM golang

RUN apt-get -y install sqlite3 libsqlite3-dev

RUN go get github.com/revel/revel
RUN go get github.com/revel/config
RUN go get github.com/revel/cmd/revel

RUN go get github.com/coopernurse/gorp
RUN go get github.com/mattn/go-sqlite3
RUN go get code.google.com/p/go.crypto/bcrypt

COPY . /go/src/bankroll

EXPOSE 9000

CMD ["revel", "run", "bankroll"]
